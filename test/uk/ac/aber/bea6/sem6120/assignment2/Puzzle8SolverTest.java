/*
 * Ben Ashwell
 * bea6@aber.ac.uk
 * SEM6120 Assignment 2
 */
package uk.ac.aber.bea6.sem6120.assignment2;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ben Ashwell
 */
public class Puzzle8SolverTest {
    
    public Puzzle8SolverTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calculateHeuristic method, of class Puzzle8Solver.
     */
    @Test
    public void testCalculateHeuristic() {
        System.out.println("calculateHeuristic");
        int[][] puzzle = {{0,2,3},
                          {1,4,5},
                          {6,7,8}};
        Puzzle8 startInstance = new Puzzle8(puzzle);
        
        int[][] goal = {{1,8,3},
                        {0,4,5},
                        {6,7,2}};
        Puzzle8 goalInstance = new Puzzle8(goal);
        Puzzle8Solver instance = new Puzzle8Solver(startInstance, goalInstance, "astar");
        int expResult = 8;
        int result = instance.calculateHeuristic(startInstance);
        assertEquals(expResult, result);
    }

    /**
     * Test of calculateHeuristic method which is complete, of class Puzzle8Solver.
     */
    @Test
    public void testCalculateHeuristicCompleteState() {
        System.out.println("calculateHeuristic");
        int[][] puzzle = {{0,2,3},
                          {1,4,5},
                          {6,7,8}};
        Puzzle8 startInstance = new Puzzle8(puzzle);        
        Puzzle8 goalInstance = new Puzzle8(puzzle);
        Puzzle8Solver instance = new Puzzle8Solver(startInstance, goalInstance, "astar");
        int expResult = 0;
        int result = instance.calculateHeuristic(startInstance);
        assertEquals(expResult, result);
    }
  
    /**
     * Method to test addToNodes method and test the ordering is correct.
     */
    @Test
    public void testAddToNodes(){
        System.out.println("AddToNodes");
        int[][] puzzle = {{0,2,3},
                          {1,4,5},
                          {6,7,8}};
        int[][] goalPuzzle = {{2,3,0},
                          {1,4,5},
                          {6,7,8}};
        Puzzle8 startInstance = new Puzzle8(puzzle);        
        Puzzle8 goalInstance = new Puzzle8(puzzle);
        Puzzle8Solver instance = new Puzzle8Solver(startInstance, goalInstance, "astar");
        
        instance.addToNodes(goalInstance);
        int expectedResult = 0;
        assertEquals(expectedResult, instance.getNodes().peek().getHeuristicValue());
    }
    
    /**
     * Test generateChildren Method with all children being created
     */
    @Test
    public void testGenerateChildrenAll(){
        System.out.println("GenerateChildren");
        int[][] puzzle = {{4,2,3},
                          {1,0,5},
                          {6,7,8}};
        Puzzle8 startInstance = new Puzzle8(puzzle);
        Puzzle8 goalInstance = new Puzzle8(puzzle);
        Puzzle8Solver instance = new Puzzle8Solver(startInstance, goalInstance, "astar");
        
        instance.generateChildren(startInstance);
        int expectedResult = 5;
        int result = instance.getNodes().size();
        assertEquals(expectedResult, result);
    }
    
    /**
     * Test GenerateChildren method with only Right and Down Children being made
     */
    @Test
    public void testGenerateChildrenRightDown(){
        System.out.println("GenerateChildren");
        int[][] puzzle = {{0,2,3},
                          {1,4,5},
                          {6,7,8}};
        Puzzle8 startInstance = new Puzzle8(puzzle);
        Puzzle8 goalInstance = new Puzzle8(puzzle);
        Puzzle8Solver instance = new Puzzle8Solver(startInstance, goalInstance, "astar");
        
        instance.generateChildren(startInstance);
        int expectedResult = 3;
        int result = instance.getNodes().size();
        assertEquals(expectedResult, result);
    }
    
    /**
     * Test generateChildren Method with only Left and Up children being made
     */
    @Test
    public void testGenerateChildrenLeftUp(){
        System.out.println("GenerateChildren");
        int[][] puzzle = {{4,2,3},
                          {1,8,5},
                          {6,7,0}};
        Puzzle8 startInstance = new Puzzle8(puzzle);
        Puzzle8 goalInstance = new Puzzle8(puzzle);
        Puzzle8Solver instance = new Puzzle8Solver(startInstance, goalInstance, "astar");
        
        instance.generateChildren(startInstance);
        int expectedResult = 3;
        int result = instance.getNodes().size();
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void testAStarSolution(){
        System.out.println("aStarSolution");
        int[][] puzzle = {{3,2,5},
                          {6,1,8},
                          {7,0,4}};
        int[][] goalPuzzle = {{0,1,2},
                          {3,4,5},
                          {6,7,8}};
        Puzzle8 startInstance = new Puzzle8(puzzle);
        Puzzle8 goalInstance = new Puzzle8(goalPuzzle);
        Puzzle8Solver instance = new Puzzle8Solver(startInstance, goalInstance, "astar");
        int result = instance.solve();
        int expectedResult = 9;
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void testAStarSolutionComplicated(){
        System.out.println("aStarSolutionComplicated");
        int[][] puzzle = {{1,2,3},
                          {4,5,6},
                          {7,8,0}};
        int[][] goalPuzzle =  {{8,6,7},
                               {2,5,4},
                              {3,0,1}};
        Puzzle8 startInstance = new Puzzle8(puzzle);
        Puzzle8 goalInstance = new Puzzle8(goalPuzzle);
        Puzzle8Solver instance = new Puzzle8Solver(startInstance, goalInstance, "astar");
        int result = instance.solve();
        int expectedResult = 31;
        assertEquals(expectedResult, result);
    }
}
