/*
 * Ben Ashwell
 * bea6@aber.ac.uk
 * SEM6120 Assignment 2
 */
package uk.ac.aber.bea6.sem6120.assignment2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ben Ashwell
 */
public class sem6120Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Puzzle8 startState = new Puzzle8(readPuzzle(args[0]));
        Puzzle8 goalState = new Puzzle8(readPuzzle(args[1]));
        Puzzle8Solver solver = new Puzzle8Solver(startState, goalState, args[2]);
        solver.solve();
        System.out.println(solver.resultToString());
    }

    private static int[][] readPuzzle(String fileName) {
        BufferedReader bufferedReader = null;
        String read = "";
        int[][] retual = new int[3][3];

        try {

            bufferedReader = new BufferedReader(new FileReader(fileName));

            for (int i = 0; i < 3; i++) {
                read = bufferedReader.readLine();
                System.out.println(read);

                String[] splitRead = read.split(",");
                for (int j = 0; j < 3; j++) {
                    retual[i][j] = Integer.parseInt(splitRead[j]);
                }
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(sem6120Main.class.getName()).log(Level.SEVERE, "Starting State File Not Found", ex);
        } catch (IOException ex) {
            Logger.getLogger(sem6120Main.class.getName()).log(Level.SEVERE, "Could not read a line from the file, is the file empty?", ex);
        }

        return retual;
    }

}
