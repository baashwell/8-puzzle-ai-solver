/*
 * Ben Ashwell
 * bea6@aber.ac.uk
 * SEM6120 Assignment 2
 */
package uk.ac.aber.bea6.sem6120.assignment2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ben Ashwell
 */
public class Puzzle8Test {
    
    public Puzzle8Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findNumberInPuzzle method, of class Puzzle8.
     */
    @Test
    public void testFindNumberInPuzzle() {
        System.out.println("findNumberInPuzzle");
        int target = 1;
        int[][] puzzle = {{0,2,3},
                          {1,4,5},
                          {6,7,8}};
        Puzzle8 instance = new Puzzle8(puzzle);
        int[] expResult = {1,0};
        int[] result = instance.findNumberInPuzzle(target);
        assertArrayEquals(expResult, result);        
    }

    /**
     * Test of findNumberInPuzzle method with incorrect target, of class Puzzle8.
     */
    @Test
    public void testFindNumberInPuzzleIncorrectTarget() {
        System.out.println("findNumberInPuzzle");
        int target = 9;
        int[][] puzzle = {{0,2,3},
                          {1,4,5},
                          {6,7,8}};
        Puzzle8 instance = new Puzzle8(puzzle);
        int[] expResult = {-1,-1};
        int[] result = instance.findNumberInPuzzle(target);
        assertArrayEquals(expResult, result);        
    }
    
    /**
     * Test of moveSpace method with the parameter up, of class Puzzle8.
     */
    @Test
    public void testMoveSpaceUp() {
        System.out.println("movePuzzle");
        String target = "up";
        int[][] puzzle = {{1,2,3},
                          {0,4,5},
                          {6,7,8}};
        Puzzle8 instance = new Puzzle8(puzzle);
        int[][] expResult = {{0,2,3},
                          {1,4,5},
                          {6,7,8}};
        Puzzle8 result = instance.moveSpace(target);
        assertArrayEquals(expResult, result.getPuzzle());        
    }
   
    /**
     * Test of moveSpace method with the parameter down, of class Puzzle8.
     */
    @Test
    public void testMoveSpaceDown() {
        System.out.println("movePuzzle");
        String target = "down";
        int[][] puzzle = {{0,2,3},
                          {1,4,5},
                          {6,7,8}};
        Puzzle8 instance = new Puzzle8(puzzle);
        int[][] expResult = {{1,2,3},
                          {0,4,5},
                          {6,7,8}};
        Puzzle8 result = instance.moveSpace(target);
        assertArrayEquals(expResult, result.getPuzzle());        
    }
    
    /**
     * Test of moveSpace method with the parameter left, of class Puzzle8.
     */
    @Test
    public void testMoveSpaceLeft() {
        System.out.println("movePuzzle");
        String target = "left";
        int[][] puzzle = {{1,2,3},
                          {4,0,5},
                          {6,7,8}};
        Puzzle8 instance = new Puzzle8(puzzle);
        int[][] expResult = {{1,2,3},
                          {0,4,5},
                          {6,7,8}};
        Puzzle8 result = instance.moveSpace(target);
        assertArrayEquals(expResult, result.getPuzzle());        
    }
    
    /**
     * Test of moveSpace method with the parameter Right, of class Puzzle8.
     */
    @Test
    public void testMoveSpaceRight() {
        System.out.println("movePuzzle");
        String target = "RIGHT";
        int[][] puzzle = {{1,2,3},
                          {0,4,5},
                          {6,7,8}};
        Puzzle8 instance = new Puzzle8(puzzle);
        int[][] expResult = {{1,2,3},
                          {4,0,5},
                          {6,7,8}};
        Puzzle8 result = instance.moveSpace(target);
        assertArrayEquals(expResult, result.getPuzzle());        
    }
    
    /**
     * Test of moveSpace method with the parameter up but at bounds, of class Puzzle8.
     */
    @Test
    public void testMoveSpaceBoundsUp() {
        System.out.println("movePuzzle");
        String target = "up";
        int[][] puzzle = {{0,2,3},
                          {1,4,5},
                          {6,7,8}};
        Puzzle8 instance = new Puzzle8(puzzle);
        int[][] expResult = null;
        Puzzle8 result = instance.moveSpace(target);
        assertEquals(expResult, result);        
    }
    
    /**
     * Test of moveSpace method with the parameter down but at bounds, of class Puzzle8.
     */
    @Test
    public void testMoveSpaceBoundsDown() {
        System.out.println("movePuzzle");
        String target = "down";
        int[][] puzzle = {{1,2,3},
                          {6,4,5},
                          {0,7,8}};
        Puzzle8 instance = new Puzzle8(puzzle);
        int[][] expResult = null;
        Puzzle8 result = instance.moveSpace(target);
        assertEquals(expResult, result);        
    }
    
    /**
     * Test of moveSpace method with the parameter left but at bounds, of class Puzzle8.
     */
    @Test
    public void testMoveSpaceBoundsLeft() {
        System.out.println("movePuzzle");
        String target = "left";
        int[][] puzzle = {{1,2,3},
                          {0,4,5},
                          {6,7,8}};
        Puzzle8 instance = new Puzzle8(puzzle);
        int[][] expResult = null;
        Puzzle8 result = instance.moveSpace(target);
        assertEquals(expResult, result);        
    }
    
    /**
     * Test of moveSpace method with the parameter Right but at bounds, of class Puzzle8.
     */
    @Test
    public void testMoveSpaceBoundsRight() {
        System.out.println("movePuzzle");
        String target = "right";
        int[][] puzzle = {{1,2,3},
                          {5,4,0},
                          {6,7,8}};
        Puzzle8 instance = new Puzzle8(puzzle);
        int[][] expResult = null;
        Puzzle8 result = instance.moveSpace(target);
        assertEquals(expResult, result);           
    }
    
    /**
     * Test of moveSpace method with the parameter Right, of class Puzzle8.
     */
    @Test
    public void testMoveSpaceIncorrect() {
        System.out.println("movePuzzle");
        String target = "incorrectParamater";
        int[][] puzzle = {{1,2,3},
                          {0,4,5},
                          {6,7,8}};
        Puzzle8 instance = new Puzzle8(puzzle);
        int[][] expResult = null;
        Puzzle8 result = instance.moveSpace(target);
        assertEquals(expResult, result);           
    }
    
     /**
     * Test of moveSpace method to check that parents are being assigned, of class Puzzle8.
     */
    @Test
    public void testMoveSpaceParent() {
        System.out.println("movePuzzle");
        String target = "left";
        int[][] puzzle = {{1,2,3},
                          {4,0,5},
                          {6,7,8}};
        Puzzle8 instance = new Puzzle8(puzzle);
        Puzzle8 result = instance.moveSpace(target);
        assertEquals(instance, result.getParent());        
    }
    
    /**
     * Test the copy Puzzle Function
     */
    @Test
    public void testCopyPuzzle(){
        System.out.println("copyPuzzle");
        int[][] puzzle = {{1,2,3},
                          {4,0,5},
                          {6,7,8}};
        Puzzle8 instance = new Puzzle8(puzzle);
        int[][] result = instance.copyPuzzle();
        Assert.assertArrayEquals(puzzle, result);
    }
    
    /**
     * Test for switch Numbers Method
     */
    @Test
    public void testSwitchNumbers(){
        System.out.println("switchNumbers");
        int[][] puzzle = {{1,2,3},
                          {4,0,5},
                          {6,7,8}};
        Puzzle8 instance = new Puzzle8(puzzle);
        int[][] expectedResult = {{1,2,3},
                          {0,4,5},
                          {6,7,8}};
        int posY = 1;
        int posX = 0;
        int newPosY = 1;
        int newPosX = 1;
        
        int[][] result = instance.switchNumbers(puzzle, posY, posX, newPosY, newPosX);
        
        Assert.assertArrayEquals(expectedResult, result);
    }

}
