/*
 * Ben Ashwell
 * bea6@aber.ac.uk
 * SEM6120 Assignment 2
 */
package uk.ac.aber.bea6.sem6120.assignment2;

import static java.util.Arrays.deepEquals;
import java.util.PriorityQueue;
import java.util.Hashtable;

/**
 *
 * @author Ben Ashwell
 */
public class Puzzle8Solver {

    private Puzzle8 goalState;
    private Puzzle8 currentState;
    private Puzzle8Comparator comparator;
    private PriorityQueue<Puzzle8> nodes;
    private Hashtable<String, Puzzle8> expandedNodes = new Hashtable<String, Puzzle8>();
    private long startTime;

    public Puzzle8Solver(Puzzle8 startState, Puzzle8 goalState, String aiTechnique) {
        this.currentState = startState;
        this.goalState = goalState;
        this.comparator = new Puzzle8Comparator(aiTechnique);
        this.nodes = new PriorityQueue<Puzzle8>(362880, comparator);
        calculateHeuristic(startState);
        nodes.add(startState);
    }

    /**
     * Calculate the heuristic value of a 8-Puzzle
     *
     * @param state The 8-Puzzle to calculate the heuristic of
     * @return The heuristic value calculated
     */
    public int calculateHeuristic(Puzzle8 state) {
        int retual = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int[] difference = goalState.findNumberInPuzzle(state.getPuzzle()[i][j]);
                int differenceCalculation = i - difference[0];

                if (differenceCalculation < 0) {
                    differenceCalculation *= -1;
                }

                retual += differenceCalculation;

                differenceCalculation = j - difference[1];

                if (differenceCalculation < 0) {
                    differenceCalculation *= -1;
                }

                retual += differenceCalculation;
            }
        }
        state.setHeuristicValue(retual);
        return retual;
    }

    /**
     * A-Star solution to loop through nodes and find a goal state.
     *
     * @return The path cost to get to this solution
     */
    public int solve() {
        startTime = System.currentTimeMillis();
        Puzzle8 currentNode;
        while (!deepEquals(nodes.peek().getPuzzle(), goalState.getPuzzle())) {
            currentNode = nodes.remove();
            if (!nodeAlreadyExplored(currentNode)) {
                generateChildren(currentNode);
                expandedNodes.put(currentNode.toString(), currentNode);
            }
        }
        return nodes.peek().getCurrentPathCost();
    }

    /**
     * Generate the children of a node and add to the list nodes
     *
     * @param node a Puzzle 8 to generate children for
     */
    public void generateChildren(Puzzle8 node) {
        addToNodes(node.moveSpace("up"));
        addToNodes(node.moveSpace("down"));
        addToNodes(node.moveSpace("left"));
        addToNodes(node.moveSpace("right"));
    }

    /**
     * Add anode to the list nodes
     *
     * @param node Puzzle 8 to add to list nodes, unless is null.
     */
    public void addToNodes(Puzzle8 node) {
        if (node != null && !nodeAlreadyExplored(node)) {
            calculateHeuristic(node);
            nodes.add(node);
        }

    }

    /**
     * Check if node is already present in the expandedNodes list, if it is
     * return true
     *
     * @param node see if this node has already been expanded
     * @return If this node has been explored
     */
    private boolean nodeAlreadyExplored(Puzzle8 node) {
        boolean retual = false;
        if (expandedNodes.containsKey(node.toString())) {
            retual = true;
        }

        return retual;
    }

    /**
     * Give result a string representation
     *
     * @return result
     */
    public String resultToString() {
        String retual = "";

        retual += "Path Cost to Solution:";
        retual += nodes.peek().getCurrentPathCost() + "\n";
        retual += "Nodes expanded: ";
        retual += expandedNodes.size() + "\n";
        retual += "Run Time: ";
        long runTime = System.currentTimeMillis() - startTime;
        retual += runTime + " milliseconds\n";

        return retual;
    }

    /**
     * Get the value of goalState
     *
     * @return the value of goalState
     */
    public Puzzle8 getGoalState() {
        return goalState;
    }

    /**
     * Set the value of goalState
     *
     * @param goalState new value of goalState
     */
    public void setGoalState(Puzzle8 goalState) {
        this.goalState = goalState;
    }

    /**
     * Get the value of currentState
     *
     * @return the value of currentState
     */
    public Puzzle8 getCurrentState() {
        return currentState;
    }

    /**
     * Set the value of currentState
     *
     * @param currentState new value of currentState
     */
    public void setCurrentState(Puzzle8 currentState) {
        this.currentState = currentState;
    }

    /**
     * Get the value of nodes, No setter as can only add to List not set it.
     *
     * @return the value of nodes
     */
    public PriorityQueue<Puzzle8> getNodes() {
        return nodes;
    }

    /**
     * Get the value of expandedNodes, No setter as can only add to list.
     *
     * @return the value of expandedNodes
     */
    public Hashtable<String, Puzzle8> getExpandedNodes() {
        return expandedNodes;
    }

}
