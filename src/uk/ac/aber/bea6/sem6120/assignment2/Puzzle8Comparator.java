/*
 * Ben Ashwell
 * bea6@aber.ac.uk
 * SEM6120 Assignment 2
 */
package uk.ac.aber.bea6.sem6120.assignment2;

import java.util.Comparator;

/**
 *
 * @author Ben Ashwell
 */
public class Puzzle8Comparator implements Comparator<Puzzle8> {

    String searchType = "";

    public Puzzle8Comparator(String searchType) {
        this.searchType = searchType;
    }

    @Override
    public int compare(Puzzle8 o1, Puzzle8 o2) {
        int retual = 0;

        if (searchType.equals("astar")) {
            if ((o1.getHeuristicValue() + o1.getCurrentPathCost()) > (o2.getHeuristicValue() + o2.getCurrentPathCost())) {
                retual = 1;
            } else if ((o1.getHeuristicValue() + o1.getCurrentPathCost()) < (o2.getHeuristicValue() + o2.getCurrentPathCost())) {
                retual = -1;
            }
        }
        else if(searchType.equals("dfs")){
            if(o1.getCurrentPathCost() > o2.getCurrentPathCost()){
                retual = -1;
            } else if(o1.getCurrentPathCost() < o2.getCurrentPathCost()){
                retual = 1;
            }
        }
        else if(searchType.equals("bfs")){
            if(o1.getCurrentPathCost() > o2.getCurrentPathCost()){
                retual = 1;
            } else if(o1.getCurrentPathCost() < o2.getCurrentPathCost()){
                retual = -1;
            }
        }
        else if (searchType.equals("gbfs")) {
            if (o1.getHeuristicValue() > o2.getHeuristicValue()) {
                retual = 1;
            } else if (o1.getHeuristicValue() < o2.getHeuristicValue()) {
                retual = -1;
            }
        }
        return retual;
    }

}
