/*
 * Ben Ashwell
 * bea6@aber.ac.uk
 * SEM6120 Assignment 2
 */
package uk.ac.aber.bea6.sem6120.assignment2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ben Ashwell
 */
public class Puzzle8ComparatorTest {
    
    public Puzzle8ComparatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class Puzzle8Comparator.
     */
    @Test
    public void testCompare() {
        System.out.println("compare");
        int[][] puzzle = {{1,2,3},
                          {4,0,5},
                          {6,7,8}};
        Puzzle8 o1 = new Puzzle8(puzzle);
        Puzzle8 o2 = new Puzzle8(puzzle);
        Puzzle8Comparator instance = new Puzzle8Comparator("astar");
        
        o1.setHeuristicValue(1);
        o2.setHeuristicValue(2);
        int expResult = -1;
        int result = instance.compare(o1, o2);
        assertEquals(expResult, result);
    }
    
       /**
     * Test of compare method, of class Puzzle8Comparator with equal puzzle8.
     */
    @Test
    public void testCompareEquals() {
        System.out.println("compare");
        int[][] puzzle = {{1,2,3},
                          {4,0,5},
                          {6,7,8}};
        Puzzle8 o1 = new Puzzle8(puzzle);
        Puzzle8 o2 = new Puzzle8(puzzle);
        Puzzle8Comparator instance = new Puzzle8Comparator("astar");
        
        o1.setHeuristicValue(1);
        o1.setCurrentPathCost(10);
        o2.setHeuristicValue(2);
        o2.setCurrentPathCost(9);
        int expResult = 0;
        int result = instance.compare(o1, o2);
        assertEquals(expResult, result);
    }
        
       /**
     * Test of compare method, of class Puzzle8Comparator with equal puzzle8.
     */
    @Test
    public void testCompareBFS() {
        System.out.println("compare");
        int[][] puzzle = {{1,2,3},
                          {4,0,5},
                          {6,7,8}};
        Puzzle8 o1 = new Puzzle8(puzzle);
        Puzzle8 o2 = new Puzzle8(puzzle);
        Puzzle8Comparator instance = new Puzzle8Comparator("bfs");
        
        o1.setCurrentPathCost(10);
        o2.setCurrentPathCost(9);
        int expResult = 1;
        int result = instance.compare(o1, o2);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of compare method, of class Puzzle8Comparator with equal puzzle8.
     */
    @Test
    public void testCompareDFS() {
        System.out.println("compare");
        int[][] puzzle = {{1,2,3},
                          {4,0,5},
                          {6,7,8}};
        Puzzle8 o1 = new Puzzle8(puzzle);
        Puzzle8 o2 = new Puzzle8(puzzle);
        Puzzle8Comparator instance = new Puzzle8Comparator("dfs");
        
        o1.setCurrentPathCost(10);
        o2.setCurrentPathCost(9);
        int expResult = -1;
        int result = instance.compare(o1, o2);
        assertEquals(expResult, result);
    }
}
