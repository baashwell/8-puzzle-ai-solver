/*
 * Ben Ashwell
 * bea6@aber.ac.uk
 * SEM6120 Assignment 2
 */
package uk.ac.aber.bea6.sem6120.assignment2;

import java.util.Arrays;

/**
 *
 * @author Ben Ashwell
 */
public class Puzzle8 {

    private int[][] puzzle;
    private int heuristicValue = 0;
    private int currentPathCost = 0;
    private Puzzle8 parent;
    private final int space = 0;

    public Puzzle8(int[][] puzzle) {
        this.puzzle = puzzle;
    }

    public Puzzle8(int[][] puzzle, Puzzle8 parent) {
        this.puzzle = puzzle;
        this.parent = parent;
        this.currentPathCost = parent.getCurrentPathCost() + 1;
    }

    /**
     * Copy the Puzzle to a new array that can be used elsewhere. 'protected' to
     * allow testing.
     *
     * @return a copy of the puzzle
     */
    protected int[][] copyPuzzle() {
        int[][] retual = new int[puzzle.length][];

        for (int i = 0; i < puzzle.length; i++) {
            retual[i] = Arrays.copyOf(puzzle[i], puzzle[i].length);
        }

        return retual;
    }

    /**
     * Switch the numbers at the location [posY,posX] and [newPosX, newPosY] of
     * the newPuzzle.
     *
     * @param newPuzzle 2D integer array puzzle to switch numbers in
     * @param posY [posY][]
     * @param posX [][posX]
     * @param newPosY [newPosY][] to switch
     * @param newPosX [][newPosX] to switch
     * @return the copied 2 Dimensional Integer Array
     */
    protected int[][] switchNumbers(int[][] newPuzzle, int posY, int posX, int newPosY, int newPosX) {
        int numberToSwitch = puzzle[posY][posX];
        newPuzzle[posY][posX] = space;
        newPuzzle[newPosY][newPosX] = numberToSwitch;

        return newPuzzle;
    }

    /**
     * Find a target number within this 8-Puzzle
     *
     * @param target The Target number to locate
     * @return The position of the target number [row, column] will be [-1,-1]
     * if not located.
     */
    public int[] findNumberInPuzzle(int target) {
        int[] retual = {-1, -1};

        beginLoop:  //Label so can break out of the entire loop once target is found
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (puzzle[i][j] == target) {
                    retual[0] = i;
                    retual[1] = j;
                    break beginLoop;
                }
            }
        }
        return retual;
    }

    /**
     * Move the space from its current location either up, down, left or right
     * and create a new Puzzle8 with the resulting puzzle if the move is valid.
     *
     * @param direction the direction to move - "up", "down", "left", "right"
     * @return the new 8-Puzzle with the updated puzzle after movement. Null if
     * the move is invalid or direction is not one of the above parameters.
     */
    public Puzzle8 moveSpace(String direction) {
        Puzzle8 retual = null;
        int[] spacePosition = this.findNumberInPuzzle(space);
        int[][] newPuzzle = copyPuzzle();

        //TODO lots of repitition here, attempt to take some of it out?
        switch (direction.toLowerCase()) {
            case "up":
                if (spacePosition[0] != 0) { //make sure can move up           
                    retual = new Puzzle8(switchNumbers(newPuzzle, spacePosition[0] - 1, spacePosition[1], spacePosition[0], spacePosition[1]), this);
                }
                break;
            case "down":
                if (spacePosition[0] != 2) { //make sure can move down
                    retual = new Puzzle8(switchNumbers(newPuzzle, spacePosition[0] + 1, spacePosition[1], spacePosition[0], spacePosition[1]), this);
                }
                break;
            case "left":
                if (spacePosition[1] != 0) { //make sure can move left
                    retual = new Puzzle8(switchNumbers(newPuzzle, spacePosition[0], spacePosition[1] - 1, spacePosition[0], spacePosition[1]), this);
                }
                break;
            case "right":
                if (spacePosition[1] != 2) { //make sure can move right=1
                    retual = new Puzzle8(switchNumbers(newPuzzle, spacePosition[0], spacePosition[1] + 1, spacePosition[0], spacePosition[1]), this);
                }
                break;
        }

        return retual;
    }

    /**
     * Get the 8 Puzzle
     *
     * @return The 2d Integer array containing the puzzle
     */
    public int[][] getPuzzle() {
        return puzzle;
    }

    /**
     * Set the 8 Puzzle
     *
     * @param puzzle
     */
    public void setPuzzle(int[][] puzzle) {
        this.puzzle = puzzle;
    }

    /**
     * Get the Heuristic value of this 8-Puzzle, Add the Parents Heuristic to
     * the value too.
     *
     * @return Heuristic integer value
     */
    public int getHeuristicValue() {
        return heuristicValue;
    }

    /**
     * Set the Heuristic value of this 8-Puzzle
     *
     * @param heuristicValue
     */
    public void setHeuristicValue(int heuristicValue) {
        this.heuristicValue = heuristicValue;
    }

    /**
     * Get this 8 Puzzles parent
     *
     * @return the Parent 8 Puzzle
     */
    public Puzzle8 getParent() {
        return parent;
    }

    /**
     * Set the parent to this 8 Puzzle
     *
     * @param parent
     */
    public void setParent(Puzzle8 parent) {
        this.parent = parent;
    }

    /**
     * Get the value of currentPathCost
     *
     * @return the value of currentPathCost
     */
    public int getCurrentPathCost() {
        return currentPathCost;
    }

    /**
     * Set the value of currentPathCost
     *
     * @param currentPathCost new value of currentPathCost
     */
    public void setCurrentPathCost(int currentPathCost) {
        this.currentPathCost = currentPathCost;
    }

    @Override
    public String toString() {
        return Arrays.deepToString(puzzle);
    }
}
